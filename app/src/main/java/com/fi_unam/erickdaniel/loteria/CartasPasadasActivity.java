package com.fi_unam.erickdaniel.loteria;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class CartasPasadasActivity extends AppCompatActivity implements View.OnClickListener {
    ListView LVcartas;
    ArrayList<Cartas> cartas;
    Button BTNcartaPasAtras, BTNGanador, BTNequivoca;
    private MediaPlayer sonido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartas_pasadas);

        // Inicializamos los objetos
        BTNcartaPasAtras = (Button) findViewById(R.id.BTNcartaPasAtras);
        BTNequivoca = (Button) findViewById(R.id.BTNequivoca);
        BTNGanador = (Button) findViewById(R.id.BTNGanador);


        // Obtenemos los datos
        cartas = getIntent().getParcelableArrayListExtra("cartasAcumuladas");


        // Inicializamos la lista y le damos estilo
        CartasAdapter adapter = new CartasAdapter(this, R.layout.litsview_item_row, cartas);
        LVcartas = (ListView) findViewById(R.id.LVcartas);
        View header = getLayoutInflater().inflate(R.layout.list_header_row, null);
        LVcartas.addHeaderView(header);
        LVcartas.setAdapter(adapter);


        // Listener
        BTNGanador.setOnClickListener(this);
        BTNequivoca.setOnClickListener(this);
        BTNcartaPasAtras.setOnClickListener(this);
        LVcartas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cartas cartas1 = (Cartas) parent.getItemAtPosition(position);

                Intent intent = new Intent(CartasPasadasActivity.this, ImagenFullscreenActivity.class);
                intent.putExtra("imagen", cartas1.getImagenId());

                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BTNcartaPasAtras:
                this.finish();
                break;
            case R.id.BTNGanador:
                Ganador();
                break;
            case R.id.BTNequivoca:
                equivocado();
                break;
        }
    }

    private void Ganador() {
        reproduce("ganador");

        Intent intent = new Intent(CartasPasadasActivity.this, ImagenFullscreenActivity.class);
        startActivity(intent);
    }

    private void equivocado() {
        reproduce("mario_lose");
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.equivocacion))
                .setMessage(getResources().getString(R.string.no_ganador))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        detiene();
                    }
                }).show();
    }

    private void reproduce(String recurso) {
        detiene();

        String uri = "@raw/" + recurso;

        try {
            int audioID = getResources().getIdentifier(uri, null, getPackageName());
            sonido = MediaPlayer.create(this, audioID);
            sonido.start();
        } catch (Exception e) {
            Log.e("Error audio", e.getMessage());
        }
    }

    private void detiene() {
        if (sonido != null)
            sonido.stop();
    }

    private void destruye() {
        if (sonido != null)
            sonido.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destruye();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        detiene();
    }
}
