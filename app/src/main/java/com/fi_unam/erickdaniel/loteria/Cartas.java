package com.fi_unam.erickdaniel.loteria;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by erick on 12/2/16.
 */

// Se implementa Parcelable para que se pueda pasar objetos de este tipo a través de intents.
public class Cartas implements Parcelable{
    private int imagenId;
    private int sonidoId;
    private String nombre;
    private int id;
    private Context context;

    public Cartas(Parcel in) {
        imagenId = in.readInt();
        sonidoId = in.readInt();
        nombre = in.readString();
        id = in.readInt();
    }

    public int getImagenId() {
        return imagenId;
    }

    public String getNombre() {
        return nombre;
    }

    public int getSonidoId(){
        return sonidoId;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setImagenId(){
        String uri = "@drawable/carta_" + id;
        imagenId = context.getResources().getIdentifier(uri, null, context.getPackageName());
    }

    private void setNombre(){
        String uri = "@string/carta_" + id;
        int idResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        nombre = context.getString(idResource);
    }

    private void setSonidoId(){
        String uri = "@raw/carta_" + id;
        sonidoId = context.getResources().getIdentifier(uri, null, context.getPackageName());
    }

    public Cartas() {
        super();
    }

    public Cartas(Context context, int id) {
        this.context = context;
        setId(id);
        setImagenId();
        setNombre();
        setSonidoId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imagenId);
        parcel.writeInt(sonidoId);
        parcel.writeString(nombre);
        parcel.writeInt(id);
    }

    public static final Parcelable.Creator<Cartas> CREATOR
            = new Parcelable.Creator<Cartas>() {
        public Cartas createFromParcel(Parcel in) {
            return new Cartas(in);
        }

        public Cartas[] newArray(int size) {
            return new Cartas[size];
        }
    };

}
