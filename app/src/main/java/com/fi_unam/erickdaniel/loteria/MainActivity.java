package com.fi_unam.erickdaniel.loteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button BTNjugar, BTNgriton, BTNsalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializamos los objetos
        BTNgriton = (Button) findViewById(R.id.BTNgriton);
        BTNjugar = (Button) findViewById(R.id.BTNjugar);
        BTNsalir = (Button) findViewById(R.id.BTNsalir);

        //Inicializamos los listeners
        BTNgriton.setOnClickListener(this);
        BTNjugar.setOnClickListener(this);
        BTNsalir.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNgriton:
                //Toast.makeText(this, "Gritón", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, GritonActivity.class);
                startActivity(intent);
                break;
            case R.id.BTNjugar:
                // Toast.makeText(this, "Jugar", Toast.LENGTH_SHORT).show();
                Intent jugar = new Intent(this, ModoJuegoActivity.class);
                startActivity(jugar);
                break;
            case R.id.BTNsalir:
                finish();
                break;
        }
    }
}
