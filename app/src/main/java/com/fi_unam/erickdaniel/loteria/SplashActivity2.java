package com.fi_unam.erickdaniel.loteria;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity2 extends AppCompatActivity {
    private static final long SPLASH_SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);

        TextView TVversion = (TextView) findViewById(R.id.version);
        String version;

        // Obtenemos información del paquete
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
            TVversion.setText("V." + version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e("Error Info Paquete", e.getMessage());
        }


        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashActivity2.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        // Simulamos la espera de 3 segundos.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}
