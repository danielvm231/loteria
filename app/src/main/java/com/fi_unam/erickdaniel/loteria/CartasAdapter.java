package com.fi_unam.erickdaniel.loteria;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by erick on 12/3/16.
 */

public class CartasAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList<Cartas> listaCartas;


    public CartasAdapter(Context context, int layoutResourceId, ArrayList<Cartas> listaCartas) {
        super(context, layoutResourceId, listaCartas);

        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.listaCartas = listaCartas;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CartasHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CartasHolder();
            holder.imagen = (ImageView) row.findViewById(R.id.imagenListView);
            holder.texto = (TextView) row.findViewById(R.id.textoListView);
            row.setTag(holder);
        } else {
            holder = (CartasHolder)row.getTag();
        }

        Cartas cartas = listaCartas.get(position);
        holder.texto.setText(cartas.getNombre());
        holder.imagen.setImageResource(cartas.getImagenId());


        return row;
    }

    static class CartasHolder {
        ImageView imagen;
        TextView texto;
    }

}
