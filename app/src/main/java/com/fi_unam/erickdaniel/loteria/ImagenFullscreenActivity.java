package com.fi_unam.erickdaniel.loteria;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

public class ImagenFullscreenActivity extends AppCompatActivity {
    private ImageView imagenFullScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen_fullscreen);

        // Recibimos los extras
        Bundle extras = getIntent().getExtras();

        imagenFullScreen = (ImageView) findViewById(R.id.imagenFullScreen);

        if (extras != null) {
            imagenFullScreen.setImageResource(extras.getInt("imagen"));
        }
        else {
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imagenFullScreen);
            Glide.with(this).load(R.raw.win).into(imagenFullScreen);
        }
    }

}
