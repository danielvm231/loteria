package com.fi_unam.erickdaniel.loteria;

class Carta {

    private String nombre;
    private int idDrawable;
    private int idDrawableGris;
    private int estatus;

    Carta(String nombre, int idDrawable, int idDrawableGris, int estatus) {
        this.nombre = nombre;
        this.idDrawable = idDrawable;
        this.idDrawableGris = idDrawableGris;
        this.estatus = estatus;
    }

    static String getNombreCarta(int numeroCarta){
        return "@string/carta_" + numeroCarta;
    }

    static String getImageUri(int numeroCarta){
        String carta = "carta_" + numeroCarta;
        return "@drawable/" + carta;
    }

    static String getImageUriGris(int numeroCarta){
        String carta = "carta_" + numeroCarta + "_dis";
        return "@drawable/" + carta;
    }

    String getNombre() {
        return this.nombre;
    }

    int getIdDrawable() {
        return this.idDrawable;
    }

    int getIdDrawableGris() {
        return this.idDrawableGris;
    }

    public int getId() {
        return nombre.hashCode();
    }

    int getEstatus() { return this.estatus; }

    void setEstatus(int estatus) { this.estatus = estatus; }
}
