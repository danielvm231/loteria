package com.fi_unam.erickdaniel.loteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class ModoJuegoActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private Button btnJugar;
    private ImageView modoJuegoImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modo_juego);

        // inicializando elementos
        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        btnJugar = (Button) findViewById(R.id.btnJugar);
        modoJuegoImagen = (ImageView) findViewById(R.id.modoJuegoImagen);

        try {
            btnJugar.setOnClickListener(this);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                   @Override
                                                   public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                                       int uriImagen;
                                                       String imagen = "@drawable/";

                                                       switch (i) {
                                                           case R.id.radioButton4:
                                                               imagen += "tablero_tradicional";
                                                               break;
                                                           case R.id.radioButton5:
                                                               imagen += "tablero_esquinas";
                                                               break;
                                                           case R.id.radioButton6:
                                                               imagen += "tablero_diagonal_izq";
                                                               break;
                                                           case R.id.radioButton7:
                                                               imagen += "tablero_diagonal_der";
                                                               break;
                                                           case R.id.radioButton8:
                                                               imagen += "tablero_linea_hor";
                                                               break;
                                                           case R.id.radioButton9:
                                                               imagen += "tablero_linea_ver";
                                                               break;
                                                           case R.id.radioButton10:
                                                               imagen += "tablero_modo_x";
                                                               break;
                                                       }

                                                       uriImagen = getResources().getIdentifier(imagen, null, getPackageName());
                                                       modoJuegoImagen.setImageResource(uriImagen);
                                                   }
                                               }


        );


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnJugar:
                // Toast.makeText(this, "Estas jugando!!", Toast.LENGTH_SHORT).show();
                Intent jugar = new Intent(this, JuegoActivity.class);
                Intent tipoJuegoLineas = new Intent(this, ModoLineasActivity.class);

                View radioButtonTablero;
                View radioButtonJuego;

                int tipoTableroId = radioGroup1.getCheckedRadioButtonId();
                int tipoJuegoId = radioGroup2.getCheckedRadioButtonId();

                radioButtonTablero = radioGroup1.findViewById(tipoTableroId);
                radioButtonJuego = radioGroup2.findViewById(tipoJuegoId);

                int tipoJuego = radioGroup2.indexOfChild(radioButtonJuego);
                int tipoTablero = radioGroup1.indexOfChild(radioButtonTablero);

                int ordenTablero = tipoTablero == 0 ? 3 : tipoTablero == 1 ? 4 : 5;
                int numeroElementos = ordenTablero * ordenTablero;

                if (tipoJuego == 4 || tipoJuego == 5) {
                    tipoJuegoLineas.putExtra("orientacion", tipoJuego);
                    tipoJuegoLineas.putExtra("orden", ordenTablero);
                    startActivity(tipoJuegoLineas);
                } else {
                    jugar.putExtra("orden", ordenTablero);
                    ArrayList<Integer> arrayIndices = new ArrayList<>();
                    switch (tipoJuego) {
                        case 0:
                            for (int i = 0; i < numeroElementos; i++) {
                                arrayIndices.add(i);
                            }
                            jugar.putExtra("indicesGanadores", arrayIndices);
                            startActivity(jugar);
                            break;
                        case 1:
                            arrayIndices.add(0);
                            arrayIndices.add(ordenTablero - 1);
                            arrayIndices.add((numeroElementos) - ordenTablero);
                            arrayIndices.add((numeroElementos) - 1);
                            jugar.putExtra("indicesGanadores", arrayIndices);
                            startActivity(jugar);
                            break;
                        case 2:
                            for (int i = 0; i < numeroElementos; i += ordenTablero + 1) {
                                arrayIndices.add(i);
                            }
                            jugar.putExtra("indicesGanadores", arrayIndices);
                            startActivity(jugar);
                            break;
                        case 3:
                            for (int i = ordenTablero - 1; i <= (numeroElementos) - ordenTablero; i += ordenTablero - 1) {
                                arrayIndices.add(i);
                            }
                            jugar.putExtra("indicesGanadores", arrayIndices);
                            startActivity(jugar);
                            break;
                        case 6:
                            for (int i = 0; i < numeroElementos; i += ordenTablero + 1) {
                                arrayIndices.add(i);
                            }
                            for (int i = ordenTablero - 1; i <= (numeroElementos) - ordenTablero; i += ordenTablero - 1) {
                                if (!arrayIndices.contains(i)) {
                                    arrayIndices.add(i);
                                }
                            }
                            jugar.putExtra("indicesGanadores", arrayIndices);
                            startActivity(jugar);
                            break;
                    }
                }
                break;
        }
    }
}
