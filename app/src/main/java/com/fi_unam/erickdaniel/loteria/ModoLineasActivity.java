package com.fi_unam.erickdaniel.loteria;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class ModoLineasActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnJugar;
    RadioGroup radioGroup;
    int orden;
    int orientacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modo_lineas);

        btnJugar = (Button) findViewById(R.id.button);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orden =  extras.getInt("orden");
            orientacion =  extras.getInt("orientacion");
            String fila = getString(R.string.fila) + " ";
            String columna = getString(R.string.columna) + " ";
            String filaColumna = orientacion == 4 ? fila : columna ;
            for (int row = 0; row < 1; row++) {
                radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
                //ll.setOrientation(LinearLayout.HORIZONTAL);
                String texto;
                for (int i = 1; i <= orden; i++) {
                    AppCompatRadioButton rdbtn = new AppCompatRadioButton(this);
                    rdbtn.setId((row * 2) + i);
                    texto = filaColumna + rdbtn.getId();
                    rdbtn.setText(texto);
                    rdbtn.setChecked(i == 1);
                    rdbtn.setTypeface(Typeface.DEFAULT_BOLD);
                    radioGroup.addView(rdbtn);
                }
                //((ViewGroup) findViewById(R.id.radiogroup)).addView(ll);
            }
        }

        try{
            btnJugar.setOnClickListener(this);
        }catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                Intent jugar = new Intent(this, JuegoActivity.class);
                int filaColumna = radioGroup.getCheckedRadioButtonId() - 1;
                jugar.putExtra("orden", orden);
                ArrayList<Integer> arrayIndices = new ArrayList<>();
                switch (orientacion){
                    case 4 :
                        for (int i = filaColumna * orden; i <= (filaColumna * orden) + (orden -1); i ++){
                            arrayIndices.add(i);
                        }
                        jugar.putExtra("indicesGanadores", arrayIndices);
                        startActivity(jugar);
                        break;
                    case 5 :
                        for (int i = filaColumna; i <= ((orden * orden) - orden) + filaColumna; i += orden){
                            arrayIndices.add(i);
                        }
                        jugar.putExtra("indicesGanadores", arrayIndices);
                        startActivity(jugar);
                        break;
                }
                break;
        }
    }
}
