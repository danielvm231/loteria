package com.fi_unam.erickdaniel.loteria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


class AdaptadorCartas extends BaseAdapter {

    private  Context context;
    private ArrayList<Carta> cartas;
    private int height;

    AdaptadorCartas(Context context, ArrayList<Carta> cartas, int height){
        this.context = context;
        this.cartas = cartas;
        this.height = height;
    }

    @Override
    public int getCount() {
        return cartas.size();
    }

    @Override
    public Carta getItem(int i) {
        return cartas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolderCarta holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
            holder = new ViewHolderCarta();
            holder.nombreCarta = (TextView) view.findViewById(R.id.nombre_carta);
            holder.imagenCarta = (ImageView) view.findViewById(R.id.imagen_carta);
            view.setTag(holder);
        }else{
            holder = (ViewHolderCarta) view.getTag();
        }

        final Carta item = getItem(position);
        int idDrawable = item.getEstatus() == 0 ? item.getIdDrawable() : item.getIdDrawableGris();
        holder.nombreCarta.setHeight(getHeight());
        Glide.with(holder.nombreCarta.getContext())
            .load(idDrawable)
            .into(holder.imagenCarta);
        holder.nombreCarta.setText(item.getNombre());
        return view;
    }

    private static class ViewHolderCarta{
        TextView nombreCarta;
        ImageView imagenCarta;
    }

    private int getHeight(){
        int alturaColumna = (int) Math.sqrt((double) cartas.size());
        //Double num = 0.0422 * height;
        //int numeroMagico = num.intValue();
        return (height / alturaColumna) - 8;
    }

}
