package com.fi_unam.erickdaniel.loteria;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Random;

public class JuegoActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    GridView grid;
    int ordenMatriz, elementosCarta;
    Carta c;
    int numCartas = 54;
    ArrayList<Integer> posicionesGanadoras;
    ArrayList<Integer> ganarAux;
    AdaptadorCartas adaptador;
    DisplayMetrics displaymetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Remove title bar
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_juego);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            posicionesGanadoras = extras.getIntegerArrayList("indicesGanadores");
            ganarAux = (ArrayList<Integer>) posicionesGanadoras.clone();
            ordenMatriz = extras.getInt("orden");
            elementosCarta = ordenMatriz * ordenMatriz;
            displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay()
                    .getMetrics(displaymetrics);
            int heightPixels = displaymetrics.heightPixels;
            int widthPixels = displaymetrics.widthPixels;
            int densityDpi = displaymetrics.densityDpi;
            float xdpi = displaymetrics.xdpi;
            float ydpi = displaymetrics.ydpi;
            Log.i("Ancho en píxeles" , "" + widthPixels);
            Log.i("Alto en píxeles" , "" + heightPixels);
            Log.i("Densidad dpi" , "" + densityDpi);
            Log.i("x dpi" , "" + xdpi);
            Log.i("y dpi" , "" + ydpi);
            int width = displaymetrics.widthPixels;
            grid = (GridView) findViewById(R.id.gridView);
            //grid.setBackgroundColor(Color.BLUE);
            grid.setNumColumns(ordenMatriz);
            grid.setColumnWidth(width / ordenMatriz);

            // Cálculamos el spacing en dpi para pasar a pixel
            final float scale = getBaseContext().getResources().getDisplayMetrics().density;
            final int dps = 2;
            int pixels = (int) (dps * scale + 0.5f);
            grid.setVerticalSpacing(pixels);
            grid.setHorizontalSpacing(pixels);
            grid.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);

            crearCartas();

            grid.setOnItemClickListener(this);
        }
    }

    public void crearCartas() {
        ArrayList<Integer> cartasNum = new ArrayList<>(elementosCarta);
        ArrayList<Carta> cartas = new ArrayList<>(elementosCarta);
        Random r = new Random();
        int siguiente = r.nextInt(numCartas) + 1;
        for (int i = 0; i < (elementosCarta); i++) {
            while (cartasNum.contains(siguiente)) {
                siguiente = r.nextInt(numCartas) + 1;
            }
            cartasNum.add(siguiente);
        }

        for (int i = 0; i < elementosCarta; i++) {
            int imageResource = getResources().getIdentifier(Carta.getImageUri(cartasNum.get(i)), null, getPackageName());
            int imageResourceGris = getResources().getIdentifier(Carta.getImageUriGris(cartasNum.get(i)), null, getPackageName());
            String nombreCarta = Carta.getNombreCarta(cartasNum.get(i));
            int nombreResource = getResources().getIdentifier(nombreCarta, null, getPackageName());
            c = new Carta(getString(nombreResource), imageResource, imageResourceGris, 0);
            cartas.add(c);
        }

        int height = displaymetrics.heightPixels;
        adaptador = new AdaptadorCartas(this, cartas, height);
        grid.setAdapter(adaptador);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int posicion, long l) {
        Carta item = (Carta) adapterView.getItemAtPosition(posicion);
        int estatus = item.getEstatus() == 1 ? 0 : 1;
        //Se verifica si el jugador ha ganado
        //Se actualiza la carta para que este seleccionada
        item.setEstatus(estatus);
        adaptador.notifyDataSetChanged();
        if (estatus == 1) {
            if (ganarAux.contains(posicion)) {
                ganarAux.remove(ganarAux.indexOf(posicion));
                if (ganarAux.isEmpty()) {
                    final MediaPlayer sonido = MediaPlayer.create(this, getResources().getIdentifier("@raw/ganador", null, getPackageName()));
                    sonido.start();
                    new AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.loteria))
                            .setMessage(getResources().getString(R.string.felicidades))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    sonido.stop();
                                }
                            })
                            .show();
                }
            }
        } else {
            if (posicionesGanadoras.contains(posicion)) {
                ganarAux.add(posicion);
            }
        }

    }
}
