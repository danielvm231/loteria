package com.fi_unam.erickdaniel.loteria;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GritonActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView imagenCarta;
    private Button BTNsiguiente, BTNacumuladas;
    private FloatingActionButton BTNmute;
    private final byte numCartas = 54;
    private boolean[] cartasPasadasMapa; //Este es el mapa de cartas libres y pasadas.
    private boolean mute;
    private byte restantes;
    private ArrayList<Cartas> cartasAcumuladas; //Esta es una lista con las cartas que han pasados.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_griton);

        // Inicializamos los objetos
        cartasAcumuladas = new ArrayList<>();
        imagenCarta = (ImageView) findViewById(R.id.imagenGriton);
        BTNsiguiente = (Button) findViewById(R.id.BTNsiguiente);
        BTNacumuladas = (Button) findViewById(R.id.BTNacumuladas);
        BTNmute = (FloatingActionButton) findViewById(R.id.BTNmute);
        restantes = numCartas;
        cartasPasadasMapa = new boolean[numCartas + 1];
        mute = false;

        // Listeners
        BTNsiguiente.setOnClickListener(this);
        BTNacumuladas.setOnClickListener(this);
        BTNmute.setOnClickListener(this);

        siguienteCarta();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BTNsiguiente:
                siguienteCarta();
                break;
            case R.id.BTNacumuladas:
                actividadCartasPasadas();
                break;
            case R.id.BTNmute:
                activaSonido();
                break;
        }
    }

    private void siguienteCarta() {
        // Generamos la siguiente imagen
        int siguiente = cartaRandom();

        //Verificamos que no se hayan acabado las cartas
        if (siguiente != 0) {
            //Marcamos la carta como usada
            quitaCartas(siguiente);

            Cartas cartaActual = new Cartas(this, siguiente);
            cartasAcumuladas.add(cartaActual);


            // Ponemos la nueva imagen. Y reproducimos el sonido
            imagenCarta.setImageResource(cartaActual.getImagenId());
            MediaPlayer sonido = MediaPlayer.create(this, cartaActual.getSonidoId());
            if (mute)
                sonido.setVolume(0.0f, 0.0f);
            sonido.start();

        } else
            Toast.makeText(this,
                    getResources().getString(R.string.no_mas_cartas),
                    Toast.LENGTH_SHORT).show();
    }

    private void quitaCartas(int carta) {
        cartasPasadasMapa[carta] = true;
        restantes--;
        Log.i("restantes", Byte.toString(restantes));
    }

    private int cartaRandom() {
        Random r = new Random();
        int siguiente;

        // Verificamos que la carta no haya pasado ya.
        do {
            siguiente = r.nextInt(numCartas) + 1;
            Log.i("Siguiente", Integer.toString(siguiente));
            Log.i("Valor", Boolean.toString(cartasPasadasMapa[siguiente]));

            // Si no quedan cartas, retorna un 0.
            if (restantes == 0) {
                siguiente = 0;
                break;
            }
        } while (cartasPasadasMapa[siguiente]);


        return siguiente;
    }

    private void imprimeMapaCartas() {
        for (int i = 0; i < numCartas + 1; i++)
            Log.i("Cartas", Integer.toString(i) + "   " + cartasPasadasMapa[i]);
    }

    private void actividadCartasPasadas() {
        Intent intent = new Intent(this, CartasPasadasActivity.class);
        intent.putExtra("cartasAcumuladas", cartasAcumuladas);

        startActivity(intent);
    }

    private void activaSonido() {
        mute = !mute;

        String uri, sonidoMensaje;
        int iconId;

        if (mute) {
            uri = "@drawable/volume_off";
            sonidoMensaje = getResources().getString(R.string.sonido_off);
            Toast.makeText(this, sonidoMensaje, Toast.LENGTH_SHORT).show();
        } else {
            uri = "@drawable/volume_high";
            sonidoMensaje = getResources().getString(R.string.sonido_on);
            Toast.makeText(this, sonidoMensaje, Toast.LENGTH_SHORT).show();
        }

        iconId = getResources().getIdentifier(uri, null, getPackageName());
        BTNmute.setImageResource(iconId);
    }
}
