# README #

### Acerca de este repositorio ###

Juego clásico de Lotería Méxicana, la alpicación fue hecha en Android para la materia de Cómputo Móvil impartida en la Facultad de Ingeniería de la UNAM.

**Tiene 2 roles:**

1. **Griton: ** Va pasando y diciendo las cartas de la lotería.
1. **Normal: ** Muestra el tablero para poder seleccionar las cartas que hayan pasado.

**Cuenta 7 modos de juego principales.**

* Tradicional
* Esquinas
* Diagonal izquierda
* Diagonal derecha
* Línea horizontal
* Línea vertical
* Equis

**Como características adicionales (extras):**

* El gritón verifica que no se hayan acabado las cartas, en dado caso que se acaben muestra un aviso.
* Se cambió el ícono por uno para la lotería mexicana.
* Se han añadido todas las cadenas al string.xml para poder traducir.
* Se creó un botón para controlar el sonido del gritón.
* Si en la lista de cartas pasadas se toca un resultado, este despliega la imagen en grande.
* Completando que se despliegue la imagen a pantalla completa en el listView.
* En el gritón se añadieron los botones de ganador y equivocación una vez que el usuario haya verificado que efectivamente hay un ganador. Este contiene una bonita animación y sonidos.
* Aplicación en español e inglés.
* Se trató de usar Material Design en los diseños.

### Versión ###

1.0

### Contacto de autores ###

**Erick Osorio Merlos: ** evgom.sid@gmail.com

**Zeuxis Daniel Villegas Moreno: ** villegasmorenodaniel@gmail.com